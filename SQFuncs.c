#include "iniparser.h"
#include "SQFuncs.h"
#include <stdio.h>

extern HSQAPI sq;
_SQUIRRELDEF( ReadIniString )
{
	SQInteger iArgCount = sq->gettop( v );
	if( iArgCount >= 4 )
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;

		sq->getstring( v, 2, &pszFile );
		sq->getstring( v, 3, &pszSection );
		sq->getstring( v, 4, &pszVar );

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushnull(v);
		else
		{
			size_t keylen = strlen(pszSection) + strlen(pszVar) + 2;
			char * pszKey = new char[keylen];
			sprintf_s(pszKey, keylen, "%s:%s", pszSection, pszVar);

			SQChar * pszValue = iniparser_getstring(pIniDict, pszKey, NULL);
			if (pIniDict == NULL)
				sq->pushnull(v);
			else
				sq->pushstring(v, pszValue, -1);

			delete[] pszKey;
			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushnull(v);

	return 1;
}

_SQUIRRELDEF( ReadIniInteger )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushnull(v);
		else
		{
			size_t keylen = strlen(pszSection) + strlen(pszVar) + 2;
			char * pszKey = new char[keylen];
			sprintf_s(pszKey, keylen, "%s:%s", pszSection, pszVar);

			int iValue = iniparser_getint(pIniDict, pszKey, NULL);
			if (pIniDict == NULL)
				sq->pushnull(v);
			else
				sq->pushinteger(v, iValue);

			delete[] pszKey;
			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushnull(v);

	return 1;
}

_SQUIRRELDEF( ReadIniNumber )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushnull(v);
		else
		{
			size_t keylen = strlen(pszSection) + strlen(pszVar) + 2;
			char * pszKey = new char[keylen];
			sprintf_s(pszKey, keylen, "%s:%s", pszSection, pszVar);

			float fValue = iniparser_getdouble(pIniDict, pszKey, NULL);
			if (pIniDict == NULL)
				sq->pushnull(v);
			else
				sq->pushfloat(v, fValue);

			delete[] pszKey;
			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushnull(v);

	return 1;
}

_SQUIRRELDEF( ReadIniBool )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushnull(v);
		else
		{
			size_t keylen = strlen(pszSection) + strlen(pszVar) + 2;
			char * pszKey = new char[keylen];
			sprintf_s(pszKey, keylen, "%s:%s", pszSection, pszVar);

			int iValue = iniparser_getboolean(pIniDict, pszKey, NULL);
			if (pIniDict == NULL)
				sq->pushnull(v);
			else
				sq->pushbool(v, iValue == 1);

			delete[] pszKey;
			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushnull(v);

	return 1;
}

inline SQInteger internal_writeIniString(HSQUIRRELVM v,
	const SQChar * pszFile,
	const SQChar * pszSection,
	const SQChar * pszVar,
	const SQChar * pszVal)
{
	dictionary * pIniDict = iniparser_load(pszFile);
	if (pIniDict == NULL)
		sq->pushbool(v, 0);
	else
	{
		size_t keylen = strlen(pszSection) + strlen(pszVar) + 2;
		char * pszKey = new char[keylen];
		sprintf_s(pszKey, keylen, "%s:%s", pszSection, pszVar);

		if (!iniparser_find_entry(pIniDict, pszSection))
		{
			if (iniparser_set(pIniDict, pszSection, NULL) == -1)
			{
				iniparser_freedict(pIniDict);
				delete[] pszKey;

				sq->pushbool(v, 0);
				return 1;
			}
		}

		if(iniparser_set(pIniDict, pszKey, pszVal) == -1)
			sq->pushbool(v, 0);
		else
		{
			FILE * pFile = fopen(pszFile, "w");
			if (pFile == NULL)
				sq->pushbool(v, 0);
			else
			{
				iniparser_dump_ini(pIniDict, pFile);
				fclose(pFile);

				sq->pushbool(v, 1);
			}
		}

		delete[] pszKey;
		iniparser_freedict(pIniDict);
	}

	return 1;
}

inline SQInteger internal_writeIniInteger(HSQUIRRELVM v,
	const SQChar * pszFile,
	const SQChar * pszSection,
	const SQChar * pszVar,
	int iVal)
{
	char szVal[33];
	sprintf_s(szVal, sizeof(szVal), "%d", iVal);

	return internal_writeIniString(v, pszFile, pszSection, pszVar, szVal);
}

inline SQInteger internal_writeIniNumber(HSQUIRRELVM v,
	const SQChar * pszFile,
	const SQChar * pszSection,
	const SQChar * pszVar,
	float fVal)
{
	char szVal[49];
	sprintf_s(szVal, sizeof(szVal), "%f", fVal);

	return internal_writeIniString(v, pszFile, pszSection, pszVar, szVal);
}

inline SQInteger internal_writeIniBoolean(HSQUIRRELVM v,
	const SQChar * pszFile,
	const SQChar * pszSection,
	const SQChar * pszVar,
	SQBool bVal)
{
	char szVal[2];
	szVal[1] = '\0';
	szVal[0] = (bVal ? '1' : '0');

	return internal_writeIniString(v, pszFile, pszSection, pszVar, szVal);
}

_SQUIRRELDEF( WriteIniString )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 5)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;
		const SQChar * pszVal;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);
		sq->getstring(v, 5, &pszVal);

		return internal_writeIniString(v, pszFile, pszSection, pszVar, pszVal);
	}
	else
	{
		sq->pushbool(v, 0);
		return 1;
	}
}

_SQUIRRELDEF( WriteIniInteger )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 5)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;
		SQInteger iVal;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);
		sq->getinteger(v, 5, &iVal);

		return internal_writeIniInteger(v, pszFile, pszSection, pszVar, iVal);
	}
	else
	{
		sq->pushbool(v, 0);
		return 1;
	}
}

_SQUIRRELDEF( WriteIniNumber )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 5)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;
		SQFloat fVal;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);
		sq->getfloat(v, 5, &fVal);

		return internal_writeIniNumber(v, pszFile, pszSection, pszVar, fVal);
	}
	else
	{
		sq->pushbool(v, 0);
		return 1;
	}
}

_SQUIRRELDEF( WriteIniBool )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 5)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;
		SQBool bVal;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);
		sq->getbool(v, 5, &bVal);

		return internal_writeIniBoolean(v, pszFile, pszSection, pszVar, bVal);
	}
	else
	{
		sq->pushbool(v, 0);
		return 1;
	}
}

_SQUIRRELDEF( CountIniSection )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 3)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushbool(v, 0);
		else
		{
			sq->pushinteger(v, iniparser_getsecnkeys(pIniDict, const_cast<char *>(pszSection)));
			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushbool(v, 0);

	return 1;
}

_SQUIRRELDEF( RemoveIniValue )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 4)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;
		const SQChar * pszVar;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);
		sq->getstring(v, 4, &pszVar);

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushbool(v, 0);
		else
		{
			size_t keyLen = strlen(pszSection) + strlen(pszVar) + 2;
			char * pszKey = new char[keyLen];
			sprintf_s(pszKey, keyLen, "%s:%s", pszSection, pszVar);
			iniparser_unset(pIniDict, pszKey);

			FILE * pFile = fopen(pszFile, "w");
			if (pFile == NULL)
				sq->pushbool(v, 0);
			else
			{
				iniparser_dump_ini(pIniDict, pFile);
				fclose(pFile);

				sq->pushbool(v, 1);
			}

			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushbool(v, 0);

	return 1;
}

_SQUIRRELDEF( DeleteIniSection )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 3)
	{
		const SQChar * pszFile;
		const SQChar * pszSection;

		sq->getstring(v, 2, &pszFile);
		sq->getstring(v, 3, &pszSection);

		dictionary * pIniDict = iniparser_load(pszFile);
		if (pIniDict == NULL)
			sq->pushbool(v, 0);
		else
		{
			iniparser_unset(pIniDict, pszSection);
			FILE * pFile = fopen(pszFile, "w");
			if (pFile == NULL)
				sq->pushbool(v, 0);
			else
			{
				iniparser_dump_ini(pIniDict, pFile);
				fclose(pFile);

				sq->pushbool(v, 1);
			}

			iniparser_freedict(pIniDict);
		}
	}
	else
		sq->pushbool(v, 0);
	
	return 1;
}

_SQUIRRELDEF( ClearIni )
{
	SQInteger iArgCount = sq->gettop(v);
	if (iArgCount >= 2)
	{
		const SQChar * pszFile;
		sq->getstring(v, 2, &pszFile);

		FILE * pFile = fopen(pszFile, "w");
		if (pFile == NULL)
			sq->pushbool(v, 0);
		else
		{
			fprintf(pFile, "\0");
			fclose(pFile);

			sq->pushbool(v, 1);
		}
	}
	else
		sq->pushbool(v, 0);

	return 1;
}

SQInteger RegisterSquirrelFunc( HSQUIRRELVM v, SQFUNCTION f, const SQChar* fname, unsigned char ucParams, const SQChar* szParams )
{
	char szNewParams[ 32 ];

	sq->pushroottable( v );
	sq->pushstring( v, fname, -1 );
	sq->newclosure( v, f, 0 ); /* create a new function */

	if ( ucParams > 0 ) 
	{
		ucParams++; /* This is to compensate for the root table */
		
		sprintf( szNewParams, "t%s", szParams );
		sq->setparamscheck( v, ucParams, szNewParams ); /* Add a param type check */
	}

	sq->setnativeclosurename( v, -1, fname );
	sq->newslot( v, -3, SQFalse );
	sq->pop( v, 1 ); /* pops the root table */

	return 0;
}

void RegisterFuncs( HSQUIRRELVM v )
{
	RegisterSquirrelFunc( v, ReadIniString, "ReadIniString", 3, "sss" );
	RegisterSquirrelFunc( v, ReadIniInteger, "ReadIniInteger", 3, "sss" );
	RegisterSquirrelFunc( v, ReadIniNumber, "ReadIniNumber", 3, "sss" );
	RegisterSquirrelFunc( v, ReadIniBool, "ReadIniBool", 3, "sss" );
	RegisterSquirrelFunc( v, WriteIniString, "WriteIniString", 4, "ssss" );
	RegisterSquirrelFunc( v, WriteIniInteger, "WriteIniInteger", 4, "sssi" );
	RegisterSquirrelFunc( v, WriteIniNumber, "WriteIniNumber", 4, "sssn" );
	RegisterSquirrelFunc( v, WriteIniBool, "WriteIniBool", 4, "sssb" );
	RegisterSquirrelFunc( v, CountIniSection, "CountIniSection", 2, "ss" );
	RegisterSquirrelFunc( v, RemoveIniValue, "RemoveIniValue", 3, "sss" );
	RegisterSquirrelFunc( v, DeleteIniSection, "DeleteIniSection", 2, "ss" );
	RegisterSquirrelFunc( v, ClearIni, "ClearIni", 1, "s" );
}
